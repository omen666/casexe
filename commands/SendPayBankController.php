<?php
/**
 * Created by PhpStorm.
 * User: omene
 * Date: 30.03.2019
 * Time: 21:05
 */

namespace app\commands;

use yii\console\ExitCode;
use yii\console\Controller;
use app\models\User;
use app\models\UserPrize;

class SendPayBankController extends Controller
{

    public function actionIndex($count_send = 1) {
        $userPrize_obj = new UserPrize();
        $userPrize = $userPrize_obj->find()
            ->select("user_prize.*,user.cart_num,user.cvs")
            ->leftJoin('user', 'user_prize.user_id = user.id')
            ->leftJoin('prize', 'user_prize.prize_id = prize.id')
            ->where('prize.type = "money" AND user_prize.use_prize=0')
            ->limit($count_send)
            ->all();
        $i = 0;
        if (!empty($userPrize)) {
            foreach ($userPrize as $prize) {
                $send_b = $prize->send_bank($prize->cart_num, $prize->cvs, $prize->value);
                if ($send_b) {
                    $i++;
                    $prize->use_prize = 1;
                    $prize->update();
                }
            }
        }
        echo 'Send ' . $i . ' payment';
        return ExitCode::OK;
    }
}