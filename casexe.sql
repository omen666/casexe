-- phpMyAdmin SQL Dump
-- version 4.6.5.2
-- https://www.phpmyadmin.net/
--
-- Хост: 127.0.0.1:3306
-- Время создания: Мар 31 2019 г., 12:53
-- Версия сервера: 10.1.19-MariaDB
-- Версия PHP: 7.0.14

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- База данных: `casexe`
--

-- --------------------------------------------------------

--
-- Структура таблицы `prize`
--

DROP TABLE IF EXISTS `prize`;
CREATE TABLE IF NOT EXISTS `prize` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `type` varchar(255) NOT NULL,
  `min` int(11) DEFAULT NULL,
  `max` int(11) DEFAULT NULL,
  `limit_present` int(11) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=4 DEFAULT CHARSET=utf8;

--
-- Дамп данных таблицы `prize`
--

INSERT INTO `prize` (`id`, `type`, `min`, `max`, `limit_present`) VALUES
(1, 'money', 1, 100, 923),
(2, 'gift_points', 1, 100, NULL),
(3, 'present', NULL, NULL, NULL);

-- --------------------------------------------------------

--
-- Структура таблицы `prize_present_items`
--

DROP TABLE IF EXISTS `prize_present_items`;
CREATE TABLE IF NOT EXISTS `prize_present_items` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `name` varchar(255) NOT NULL,
  `ru_name` varchar(255) NOT NULL,
  `limit_present` int(11) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=4 DEFAULT CHARSET=utf8;

--
-- Дамп данных таблицы `prize_present_items`
--

INSERT INTO `prize_present_items` (`id`, `name`, `ru_name`, `limit_present`) VALUES
(1, 'phone', 'Телефон', 4),
(2, 'computer', 'Компьютер', 5),
(3, 'laptop', 'Ноутбук', 4);

-- --------------------------------------------------------

--
-- Структура таблицы `user`
--

DROP TABLE IF EXISTS `user`;
CREATE TABLE IF NOT EXISTS `user` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `username` varchar(255) NOT NULL,
  `password_hash` varchar(255) NOT NULL,
  `password_reset_token` varchar(255) NOT NULL,
  `email` varchar(255) NOT NULL,
  `auth_key` varchar(255) NOT NULL,
  `status` int(11) NOT NULL,
  `created_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  `updated_at` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00',
  `password` varchar(255) NOT NULL,
  `gift_points` int(11) NOT NULL,
  `cart_num` int(11) NOT NULL,
  `cvs` int(11) NOT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `id` (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=4 DEFAULT CHARSET=utf8;

--
-- Дамп данных таблицы `user`
--

INSERT INTO `user` (`id`, `username`, `password_hash`, `password_reset_token`, `email`, `auth_key`, `status`, `created_at`, `updated_at`, `password`, `gift_points`, `cart_num`, `cvs`) VALUES
(1, 'admin', '$2y$13$8O0QYoj2JTN84QqQ89TaOesyibaH0eTbHvjykO3NHXa.0GswOzoUq', '', 'admin@кmail.ru', 'gvtx8ssSuhGLX7PMcEAHNMxYMC1w6Fkt', 10, '2019-03-31 09:52:28', '0000-00-00 00:00:00', '', 109, 123, 444),
(3, 'test', '$2y$13$ZxyWHxdYIPCUd9N15FG/n.0dz592PkbwnsGh5rmTIi7v9EEIrFaW6', '', 'test@mail.ru', 'wo7Va5ttlNvcdkMeTqVvHxlGuPCIEx6C', 10, '2019-03-30 16:47:37', '0000-00-00 00:00:00', '', 0, 123123, 555);

-- --------------------------------------------------------

--
-- Структура таблицы `user_prize`
--

DROP TABLE IF EXISTS `user_prize`;
CREATE TABLE IF NOT EXISTS `user_prize` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `user_id` int(11) NOT NULL,
  `prize_id` int(11) NOT NULL,
  `value` int(11) NOT NULL,
  `present_prize_id` int(11) DEFAULT NULL,
  `use_prize` tinyint(4) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=13 DEFAULT CHARSET=utf8;

--
-- Дамп данных таблицы `user_prize`
--

INSERT INTO `user_prize` (`id`, `user_id`, `prize_id`, `value`, `present_prize_id`, `use_prize`) VALUES
(7, 1, 3, 0, 3, 0),
(8, 1, 2, 89, NULL, 1),
(9, 1, 2, 81, NULL, 0),
(10, 1, 3, 0, 1, 1),
(11, 1, 1, 10, NULL, 1),
(12, 1, 1, 67, NULL, 0);

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
