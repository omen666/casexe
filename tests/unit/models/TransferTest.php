<?php
/**
 * Created by PhpStorm.
 * User: omene
 * Date: 31.03.2019
 * Time: 13:42
 */

namespace tests\unit\models;
require __DIR__ . '/../../../models/UserPrize.php';

use PHPUnit\Framework\TestCase;
use app\models\UserPrize;

class TransferTest extends TestCase
{

    public function testTransfer() {
        $userPrize = new UserPrize();

        for ($i = 1; $i <= 100; $i++) {
            $summ = rand(1, 1000);
            $expected = $userPrize->transfer($summ, $userPrize::COEFF);
            $this->assertEquals($expected, $summ * $userPrize::COEFF);
        }

    }
}