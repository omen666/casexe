<?php

namespace app\controllers;

use Yii;
use yii\filters\AccessControl;
use yii\web\Controller;
use yii\web\Response;
use yii\filters\VerbFilter;
use app\models\LoginForm;
use app\models\ContactForm;
use app\models\User;
use app\models\UserPrize;
use app\models\Prize;

class SiteController extends Controller
{
    /**
     * {@inheritdoc}
     */
    public function behaviors() {
        return [
            'access' => [
                'class' => AccessControl::className(),
                'only' => ['logout'],
                'rules' => [
                    [
                        'actions' => ['logout'],
                        'allow' => true,
                        'roles' => ['@'],
                    ],
                ],
            ],
            'verbs' => [
                'class' => VerbFilter::className(),
                'actions' => [
                    'logout' => ['post'],
                ],
            ],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function actions() {
        return [
            'error' => [
                'class' => 'yii\web\ErrorAction',
            ],
            'captcha' => [
                'class' => 'yii\captcha\CaptchaAction',
                'fixedVerifyCode' => YII_ENV_TEST ? 'testme' : null,
            ],
        ];
    }

    /**
     * Displays homepage.
     *
     * @return string
     */
    public function actionIndex() {
        return $this->render('index');
    }

    /**
     * Login action.
     *
     * @return Response|string
     */
    public function actionLogin() {
        if (!Yii::$app->user->isGuest) {
            return $this->goHome();
        }

        $model = new LoginForm();
        if ($model->load(Yii::$app->request->post()) && $model->login()) {
            return $this->goBack();
        }

        $model->password = '';
        return $this->render('login', [
            'model' => $model,
        ]);
    }

    /**
     * Logout action.
     *
     * @return Response
     */
    public function actionLogout() {
        Yii::$app->user->logout();

        return $this->goHome();
    }

    /**
     * Displays contact page.
     *
     * @return Response|string
     */
    public function actionContact() {
        $model = new ContactForm();
        if ($model->load(Yii::$app->request->post()) && $model->contact(Yii::$app->params['adminEmail'])) {
            Yii::$app->session->setFlash('contactFormSubmitted');

            return $this->refresh();
        }
        return $this->render('contact', [
            'model' => $model,
        ]);
    }

    /**
     * Displays about page.
     *
     * @return string
     */
    public function actionAbout() {
        return $this->render('about');
    }

    public function actionGet_prize() {
        $db = Yii::$app->getDb();
        $prize_obj = new Prize();
        $prize = $prize_obj->get_random_prize();
        $userPrize = new UserPrize();
        $userPrize->user_id = Yii::$app->user->id;
        $userPrize->prize_id = $prize['id'];
        $userPrize->value = $prize['value'];
        $userPrize->present_prize_id = $prize['prize_present_item_id'];
        $userPrize->save();
        $userPrizeId = $userPrize->id;
        switch ($prize['type']) {
            case 'money':
                $db->createCommand('UPDATE prize SET limit_present=limit_present-' . $prize['value'] . ' WHERE id=' . $prize['id'])->execute();
                echo $this->renderPartial('money_prize', [
                    'prize' => $prize,
                    'userPrizeId' => $userPrizeId,
                    'coef' => $userPrize::COEFF
                ]);
                break;
            case 'gift_points':
                echo $this->renderPartial('gift_points_prize', [
                    'prize' => $prize,
                    'userPrizeId' => $userPrizeId,
                ]);
                break;
            case 'present':
                $db->createCommand('UPDATE prize_present_items SET limit_present=limit_present-1 WHERE id=' . $prize['prize_present_item_id'])->execute();
                echo $this->renderPartial('present_prize', [
                    'prize' => $prize,
                    'userPrizeId' => $userPrizeId,
                ]);
                break;
        }
        die();
    }

    public function actionRenouncement($id) {
        $db = Yii::$app->getDb();
        $userPrize_obj = new UserPrize();
        $userPrize = $db->createCommand("SELECT t.*,
                                                     p.type,
                                                     p.id as prize_id 
                                              FROM user_prize as t
                                              LEFT JOIN prize as p ON t.prize_id = p.id
                                              WHERE t.id=" . $id)->queryOne();

        switch ($userPrize['type']) {
            case 'money':
                $db->createCommand('UPDATE prize SET limit_present=limit_present+' . $userPrize['value'] . ' WHERE id=' . $userPrize['prize_id'])->execute();
                break;
            case 'present':
                $db->createCommand('UPDATE prize_present_items SET limit_present=limit_present+1 WHERE id=' . $userPrize['present_prize_id'])->execute();
                break;
        }

        $userPrize_obj->deleteAll('id=' . $id);
        die();
    }

    public function actionSend_bank($id) {
        $userPrize_obj = new UserPrize();
        $userPrize = $userPrize_obj->find()
            ->select("user_prize.*,user.cart_num,user.cvs")
            ->leftJoin('user', 'user_prize.user_id = user.id')
            ->leftJoin('prize', 'user_prize.prize_id = prize.id')
            ->where('user_prize.id=' . $id)
            ->one();
        $send_b = $userPrize->send_bank($userPrize->cart_num, $userPrize->cvs, $userPrize->value);
        if ($send_b) {
            $userPrize->use_prize = 1;
            $userPrize->update();
        }
        die;
    }

    public function actionTransfer($id) {
        $prize_obj = new Prize();
        $prize = $prize_obj->findOne(['type' => 'money']);

        $userPrize_obj = new UserPrize();
        $userPrize = $userPrize_obj->findIdentity($id);
        $userPrize->value = $userPrize->transfer($userPrize->value);
        $userPrize->prize_id = $prize->id;
        $userPrize->use_prize = 1;
        $userPrize->update();

        $user_obj = new User();
        $user = $user_obj->findOne(['id' => Yii::$app->user->id]);
        $user->gift_points += $userPrize->value;
        $user->update();
    }

    public function actionScore_points($id) {
        $userPrize_obj = new UserPrize();
        $userPrize = $userPrize_obj->findIdentity($id);
        $userPrize->use_prize = 1;
        $userPrize->update();

        $user_obj = new User();
        $user = $user_obj->findOne(['id' => Yii::$app->user->id]);
        $user->gift_points += $userPrize->value;
        $user->update();
    }

    public function actionPick_up($id) {
        $userPrize_obj = new UserPrize();
        $userPrize = $userPrize_obj->findIdentity($id);
        $userPrize->use_prize = 1;
        $userPrize->update();
    }

    // Для создания пользователей

    /*    public function actionAddAdmin() {
            $model = User::find()->where(['username' => 'test'])->one();
            if (empty($model)) {
                $user = new User();
                $user->username = 'test';
                $user->email = 'test@mail.ru';
                $user->setPassword('test');
                $user->generateAuthKey();
                if ($user->save()) {
                    echo 'good';
                }
            }
        }*/

}
