<?php
/**
 * Created by PhpStorm.
 * User: omene
 * Date: 30.03.2019
 * Time: 10:52
 */
?>
<div class="row prize_block">
    <div class="col-md-3"></div>
    <div class="col-md-6">
        <h2>Ваш приз Деньги</h2>
        <h3>Сумма: <?= number_format($prize['value'], 0, ',', ' '); ?> руб.</h3>
        <div class="row">
            <div class="col-md-2">
                <button class="btn btn-sm btn-default" onclick="renouncement(<?= $userPrizeId ?>);">Отказаться</button>
            </div>
            <div class="col-md-4">
                <button class="btn btn-sm btn-info" onclick="send_bank(<?= $userPrizeId ?>);">Перечислить на счет в
                    банке
                </button>
            </div>
            <div class="col-md-4">
                <button class="btn btn-sm btn-success" onclick="transfer(<?= $userPrizeId ?>)">Перевести в балы по курсу
                    1 к <?= $coef; ?></button>
            </div>
        </div>
    </div>
    <div class="col-md-3"></div>
</div>
