<?php

/* @var $this yii\web\View */

$this->title = 'Розыгрыш призов';
?>
<div class="site-index">

    <div class="jumbotron">
        <h1>Розыгрыш призов</h1>
        <?if(!Yii::$app->user->isGuest){?>
            <p><a class="btn btn-lg btn-success" onclick="get_prize();">Получить приз</a></p>
        <?}else{?>
            <p>Вам нужно <a href="/web/index.php?r=site/login">авторизоваться</a> </p>
        <?}?>
    </div>

</div>
