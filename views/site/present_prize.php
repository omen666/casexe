<?php
/**
 * Created by PhpStorm.
 * User: omene
 * Date: 30.03.2019
 * Time: 10:52
 */
?>
<div class="row prize_block">
    <div class="col-md-4"></div>
    <div class="col-md-4">
        <h2>Ваш приз <?= $prize['value'] ?></h2>
        <div class="row">
            <div class="col-md-6">
                <button class="btn btn-sm btn-default" onclick="renouncement(<?=$userPrizeId?>);">Отказаться</button>
            </div>
            <div class="col-md-6">
                <button class="btn btn-sm btn-info" onclick="pick_up(<?=$userPrizeId?>);">Забрать</button>
            </div>
        </div>
    </div>
    <div class="col-md-4"></div>
</div>