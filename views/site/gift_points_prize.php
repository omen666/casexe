<?php
/**
 * Created by PhpStorm.
 * User: omene
 * Date: 30.03.2019
 * Time: 10:52
 */
?>
<div class="row prize_block">
    <div class="col-md-4"></div>
    <div class="col-md-4">
        <h2>Ваш приз бонусные балы</h2>
        <h3>Сумма: <?= number_format($prize['value'],0,',',' '); ?></h3>
        <div class="row">
            <div class="col-md-6">
                <button class="btn btn-sm btn-default" onclick="renouncement(<?=$userPrizeId?>);">Отказаться</button>
            </div>
            <div class="col-md-6">
                <button class="btn btn-sm btn-info" onclick="score_points(<?=$userPrizeId?>)">Зачислить на ваш счет</button>
            </div>
        </div>
    </div>
    <div class="col-md-4"></div>
</div>
