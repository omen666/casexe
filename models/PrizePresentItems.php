<?php
/**
 * Created by PhpStorm.
 * User: omene
 * Date: 30.03.2019
 * Time: 11:59
 */

namespace app\models;

use Yii;
use yii\db\ActiveRecord;

class PrizePresentItems extends ActiveRecord
{
    public static function tableName()
    {
        return '{{%prize_present_items}}';
    }
}