<?php
/**
 * Created by PhpStorm.
 * User: omene
 * Date: 30.03.2019
 * Time: 10:11
 */

namespace app\models;

use Yii;
use yii\db\ActiveRecord;

class UserPrize extends ActiveRecord
{


    const COEFF = 2;
    public $cart_num;
    public $cvs;

    public static function tableName() {
        return '{{%user_prize}}';
    }

    public static function findIdentity($id) {
        return static::findOne(['id' => $id]);
    }

    public function send_bank($cartnum, $code, $summ) {
        $url = 'https://sberbank.ru/payment/api?cartnum=' . $cartnum . '&code=' . $code . '&summ=' . $summ;
        $curl = curl_init();
        curl_setopt($curl, CURLOPT_URL, $url);
        curl_setopt($curl, CURLOPT_RETURNTRANSFER, 1);
        curl_setopt($curl, CURLOPT_HEADER, 0);
        //$result = curl_exec($curl);
        //curl_close($curl);
        //$res = json_decode($result, true);
        if (empty($res['error'])) {
            return true;
        }
        return false;
    }

    public function transfer($summ) {
        return ceil($summ * self::COEFF);
    }
}