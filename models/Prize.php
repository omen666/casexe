<?php
/**
 * Created by PhpStorm.
 * User: omene
 * Date: 30.03.2019
 * Time: 11:29
 */

namespace app\models;

use Yii;
use yii\db\ActiveRecord;
use app\models\PrizePresentItems;

class Prize extends ActiveRecord
{

    public static function tableName() {
        return '{{%prize}}';
    }

    public function get_random_prize() {

        $prizes = $this->chek_prizes();

        $prize = $prizes[array_rand($prizes)];
        $prize_present_item_id = '';
        switch ($prize['type']) {
            case 'money':
                if ($prize['limit_present'] > $prize['max']) {
                    $max = $prize['max'];
                } else {
                    $max = $prize['limit_present'];
                }
                $value = rand($prize['min'], $max);
                break;
            case 'gift_points':
                $value = rand($prize['min'], $prize['max']);
                break;
            case 'present':
                $value = $prize['items'][array_rand($prize['items'])]['ru_name'];
                $prize_present_item_id = $prize['items'][array_rand($prize['items'])]['id'];
                break;
        }
        return
            [
                'id' => $prize['id'],
                'type' => $prize['type'],
                'value' => $value,
                'prize_present_item_id' => $prize_present_item_id
            ];
    }

    public function chek_prizes() {
        $connection = Yii::$app->getDb();
        $command = $connection->createCommand("SELECT * FROM prize");
        $prizes = $command->queryAll();
        foreach ($prizes as $key => $prize) {
            if ($prize['type'] == 'money' && empty($prize['limit_present'])) {
                unset($prizes[$key]);
                continue;
            }
            if ($prize['type'] == 'present') {
                $command = $connection->createCommand("SELECT * FROM prize_present_items WHERE limit_present>0");
                $prize_present_items = $command->queryAll();
                if (!empty($prize_present_items)) {
                    $prizes[$key]['items'] = $prize_present_items;
                } else {
                    unset($prizes[$key]);
                    continue;
                }
            }
        }

        return $prizes;

    }
}