/**
 * Created by omene on 30.03.2019.
 */


function get_prize() {
    $.ajax({
        url: '/web/index.php?r=site/get_prize',
        dataType: 'html',
        success: function (rdata) {
            $('.prize_block').remove();
            $('.jumbotron').after(rdata);
        }
    });
}

function renouncement(id) {
    $.ajax({
        url: '/web/index.php?r=site/renouncement',
        type: "GET",
        dataType: 'html',
        data: {
            'id': id,
        },
        success: function (rdata) {
            $('.prize_block').remove();
        }
    });
}

function send_bank(id) {
    $.ajax({
        url: '/web/index.php?r=site/send_bank',
        type: "GET",
        dataType: 'html',
        data: {
            'id': id,
        },
        success: function (rdata) {
            $('.prize_block').remove();
        }
    });
}

function transfer(id) {
    $.ajax({
        url: '/web/index.php?r=site/transfer',
        type: "GET",
        dataType: 'html',
        data: {
            'id': id,
        },
        success: function (rdata) {
            $('.prize_block').remove();
        }
    });
}

function score_points(id) {
    $.ajax({
        url: '/web/index.php?r=site/score_points',
        type: "GET",
        dataType: 'html',
        data: {
            'id': id,
        },
        success: function (rdata) {
            $('.prize_block').remove();
        }
    });
}

function pick_up(id) {
    $.ajax({
        url: '/web/index.php?r=site/pick_up',
        type: "GET",
        dataType: 'html',
        data: {
            'id': id,
        },
        success: function (rdata) {
            $('.prize_block').remove();
        }
    });
}